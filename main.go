package main

import (
	"bufio"
	"flag"
	"goken2/cmd"
	"goken2/config"
	"goken2/files"
	"goken2/info"
	"goken2/logging"
	"goken2/model"
	"goken2/utils"
	"os"
	"time"
)

/* These are default ldflag fields set to main.* by goreleaser, see default ldflags */
/* https://goreleaser.com/customization/build/ */
var version string
var commit string
var date string
var builtBy string

// GlobalConfig will hold the global configuration state for the program.
var GlobalConfig config.Config

type invalidArgsError struct {
	Message string
}

func (e invalidArgsError) Error() string {
	return e.Message
}

func main() {

	performSetup()

	awsCli := cmd.Cli{GokenConfig: GlobalConfig}
	printer := info.Printer{GokenConfig: GlobalConfig}

	clientId, clientSecret, _ := awsCli.RegisterClient()
	logging.Debug.Printf("Successfully retrieved clientId[%s] and clientSecret[%s]", clientId, clientSecret)
	deviceCode, userCode, verificationUriComplete, _ := awsCli.StartDeviceAuthorization(clientId, clientSecret)
	logging.Debug.Printf("Successfully retrieved deviceCode[%s] and verificationUriComplete[%s]", deviceCode, verificationUriComplete)

	awsCli.OpenBrowser(verificationUriComplete)
	logging.Message.Printf("Code: %s\nPlease authorize in browser and press 'Enter' to continue...\n", userCode)
	_, _ = bufio.NewReader(os.Stdin).ReadBytes('\n')

	logging.Debug.Print("Successfully accessed verificationUriComplete")
	accessToken, _ := awsCli.CreateToken(clientId, clientSecret, deviceCode)
	logging.Debug.Printf("Successfully retrieved accessToken[%s]", accessToken)

	accounts, _ := awsCli.ListAccounts(accessToken)
	logging.Debug.Printf("Successfully retrieved accounts[%s]", accounts)

	accountRoles, _ := awsCli.ListAccountRoles(accessToken, accounts)
	logging.Debug.Printf("Successfully retrieved accountRoles[%s]", accountRoles)

	stsCredentials, _ := awsCli.GetRoleCredentials(accessToken, accountRoles)
	logging.Debug.Printf("Successfully retrieved stsCredentials[%s]", stsCredentials)

	writeCredentials(stsCredentials)
	printer.Info(stsCredentials)

}

func performSetup() {
	// parse CLI arguments
	GlobalConfig = parseArguments()
	// initialize logging resources
	logging.InitLogging(GlobalConfig)
	// conditionally clear files if clear flag set
	files.Clear(GlobalConfig)
	// print version if version or debug flag set
	info.GokenVersion(GlobalConfig)
}

func parseArguments() config.Config {

	versionFlagPtr := flag.Bool("version", false, "Print version")
	debugFlagPtr := flag.Bool("debug", false, "Run in debug mode")
	silentFlagPtr := flag.Bool("silent", false, "Run in silent mode")
	adminFlagPtr := flag.Bool("admin", false, "If you have admin creds, include in the login")
	clearFlagPtr := flag.Bool("clear", false, "Clear out existing files created by Goken")
	accountNameFlagPtr := flag.Bool("account-name", false, "Set profiles as accountName_role, rather than accountId_role")

	var usernameString string
	flag.StringVar(&usernameString, "username", "", "Supply username in command versus input")

	var regionString string
	flag.StringVar(&regionString, "region", "", "Specify the AWS region to use")

	flag.Parse()

	utils.Check(invalidArgs(debugFlagPtr, silentFlagPtr))

	var gokenConfig = config.New()

	if *debugFlagPtr {
		gokenConfig.DebugMode = true
		gokenConfig.SilentMode = false
	} else if *silentFlagPtr {
		gokenConfig.DebugMode = false
		gokenConfig.SilentMode = true
	}

	if *versionFlagPtr {
		gokenConfig.PrintVersion = true
	}

	if *adminFlagPtr {
		gokenConfig.AdminCreds = true
	}

	if *clearFlagPtr {
		gokenConfig.ClearFiles = true
	}

	if *accountNameFlagPtr {
		gokenConfig.AccountName = true
	}

	if usernameString != "" {
		gokenConfig.Username = usernameString
	}

	if regionString != "" {
		gokenConfig.Region = regionString
	}

	/* build related fields */

	if version != "" {
		gokenConfig.BuildVersion = version
	}

	if commit != "" {
		gokenConfig.BuildCommit = commit
	}

	if date != "" {
		gokenConfig.BuildDate = date
	}

	if builtBy != "" {
		gokenConfig.BuiltBy = builtBy
	}

	return gokenConfig

}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	logging.Profile.Printf("%s took %s\n", name, elapsed)
}

func writeCredentials(credentials []model.StsCredential) {

	defer timeTrack(time.Now(), "writing files")

	writeErr := files.WriteConfigAndCredentials(credentials, GlobalConfig)
	if writeErr != nil {
		// try to create parent directory and/or file?
		logging.Error.Println("problem writing credentials ", writeErr.Error())
		os.Exit(-1)
	}
}

func invalidArgs(debugFlagPtr *bool, silentFlagPtr *bool) error {
	if *debugFlagPtr && *silentFlagPtr {
		return &invalidArgsError{
			Message: "Debug and Silent modes are mutually exclusive, please set only one",
		}
	}

	return nil
}
