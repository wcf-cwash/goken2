package cmd

import (
	"encoding/json"
	"fmt"
	"goken2/config"
	"goken2/logging"
	"goken2/model"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"golang.org/x/sync/errgroup"
)

// TODO: move this to a way for other organizations to configure
const startUrl = "https://wcf.awsapps.com/start"

type Cli struct {
	GokenConfig config.Config
}

// AwsVersion will execute the version command of the AWS CLI
func AwsVersion() (string, error) {
	versionCmd := New("aws")
	versionCmd.WithArg("--version")
	output, err := versionCmd.Output()
	if err != nil {
		return "", fmt.Errorf("error running aws version: %s", err)
	}
	return firstLine(output), nil
}

func (a *Cli) AwsCmd(args ...string) *Cmd {
	cmd2 := New("aws")
	cmd2.WithArg("--region")
	cmd2.WithArg(a.GokenConfig.Region)

	for _, a := range args {
		cmd2.WithArg(a)
	}

	return cmd2
}

func (a *Cli) AwsCmdJson(args ...string) *Cmd {
	cmd2 := New("aws")

	cmd2.WithArg("--region")
	cmd2.WithArg(a.GokenConfig.Region)

	for _, a := range args {
		cmd2.WithArg(a)
	}

	cmd2.WithArg("--output")
	cmd2.WithArg("json")

	return cmd2
}

// RegisterClient will call the register-client command on AWS
func (a *Cli) RegisterClient() ( /*clientId*/ string /*clientSecret*/, string, error) {
	if os.Getenv("AWS_PROFILE") == "" {
		err := os.Unsetenv("AWS_PROFILE")
		if err != nil {
			return "", "", fmt.Errorf("error unsetting AWS_PROFILE environment variable: %s", err)
		}
	}
	registerClientCmd := a.AwsCmdJson("sso-oidc", "register-client", "--client-type", "public", "--client-name", "awscli2")
	jsonOutput, err := registerClientCmd.Output()
	if err != nil {
		return "", "", fmt.Errorf("error running aws sso-oidc register-client: %s", err)
	}
	registerClientMap := parseJson(jsonOutput)
	clientId := registerClientMap["clientId"].(string)
	clientSecret := registerClientMap["clientSecret"].(string)

	return clientId, clientSecret, nil
}

// StartDeviceAuthorization will call the start-device-authorization command on AWS
func (a *Cli) StartDeviceAuthorization(clientId string, clientSecret string) ( /* deviceCode */ string /* userCode */, string /* verificationUriComplete */, string, error) {
	startDeviceCmd := a.AwsCmdJson("sso-oidc", "start-device-authorization", "--client-id", clientId, "--client-secret", clientSecret, "--start-url", startUrl)
	jsonOutput, err := startDeviceCmd.Output()
	if err != nil {
		return "", "", "", fmt.Errorf("error running aws sso-oidc start-device-authorization: %s", err)
	}
	startDeviceMap := parseJson(jsonOutput)
	deviceCode := startDeviceMap["deviceCode"].(string)
	userCode := startDeviceMap["userCode"].(string)
	verificationUriComplete := startDeviceMap["verificationUriComplete"].(string)

	return deviceCode, userCode, verificationUriComplete, nil
}

// OpenBrowser will open a browser to verify the sign in
func (a *Cli) OpenBrowser(url string) {
	var err error

	url = checkPrefixHasHttps(url)

	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		// was popping Windows Explorer open, not a browser
		// err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
		err = exec.Command("cmd", "/c", "start", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		logging.Error.Fatal(err)
	}
}

// CreateToken will call create-token command on AWS
func (a *Cli) CreateToken(clientId string, clientSecret string, deviceCode string) (string, error) {
	createTokenCmd := a.AwsCmdJson("sso-oidc", "create-token", "--client-id", clientId, "--client-secret", clientSecret, "--grant-type", "urn:ietf:params:oauth:grant-type:device_code", "--device-code", deviceCode)
	jsonOutput, err := createTokenCmd.Output()
	if err != nil {
		return "", fmt.Errorf("error running aws sso-oidc create-token: %s", err)
	}
	createTokenMap := parseJson(jsonOutput)
	accessToken := createTokenMap["accessToken"].(string)

	return accessToken, nil
}

// ListAccounts will call list-account command on AWS
func (a *Cli) ListAccounts(accessToken string) ( /* accountIds */ []model.Account, error) {
	listAccountsCmd := a.AwsCmdJson("sso", "list-accounts", "--access-token", accessToken)
	jsonOutput, err := listAccountsCmd.Output()
	if err != nil {
		return nil, fmt.Errorf("error running aws sso list-accounts: %s", err)
	}
	var accounts model.Accounts
	parseAccounts(jsonOutput, &accounts)

	return accounts.Accounts, nil
}

// ListAccountRoles will call list-account-roles command on AWS
func (a *Cli) ListAccountRoles(accessToken string, accounts []model.Account) ([]model.AccountRole, error) {
	var accountRoleSlice []model.AccountRole
	for _, account := range accounts {
		listAccountRolesCmd := a.AwsCmdJson("sso", "list-account-roles", "--access-token", accessToken, "--account-id", account.ID)
		jsonOutput, err := listAccountRolesCmd.Output()
		if err != nil {
			return nil, fmt.Errorf("error running aws sso list-account-roles: %s", err)
		}
		var accountRoles model.AccountRoles
		parseAccountRoles(jsonOutput, &accountRoles)

		for _, accountRole := range accountRoles.AccountRoles {
			if !(strings.Contains(accountRole.RoleName, "AdministratorAccess") || strings.Contains(accountRole.RoleName, "BreakGlass")) {
				accountRoleSlice = a.appendRole(accountRole, account, accountRoleSlice)
			} else if a.GokenConfig.AdminCreds {
				accountRoleSlice = a.appendRole(accountRole, account, accountRoleSlice)
			}
			// otherwise don't include
		}

	}

	return accountRoleSlice, nil
}

func (a *Cli) appendRole(accountRole model.AccountRole, account model.Account, accountRoleSlice []model.AccountRole) []model.AccountRole {
	accountRole.AccountID = account.ID
	accountRole.AccountName = account.Name
	accountRoleSlice = append(accountRoleSlice, accountRole)
	return accountRoleSlice
}

// GetRoleCredentials will call get-role-credentials on AWS
func (a *Cli) GetRoleCredentials(accessToken string, accountRoles []model.AccountRole) ([]model.StsCredential, error) {

	stsCredentialSlice := make([]model.StsCredential, len(accountRoles))
	var g errgroup.Group

	for i, accountRole := range accountRoles {
		i, accountRole := i, accountRole

		g.Go(func() error {
			getRoleCredentialsCmd := a.AwsCmdJson("sso", "get-role-credentials", "--account-id", accountRole.AccountID, "--role-name", accountRole.RoleName, "--access-token", accessToken)
			jsonOutput, err := getRoleCredentialsCmd.Output()

			if err != nil {
				return err
			}

			var stsCredential model.StsCredential
			parseStsCredential(jsonOutput, &stsCredential)
			if a.GokenConfig.AccountName {
				stsCredential.Profile = accountRole.AccountName + "_" + accountRole.RoleName
			} else {
				stsCredential.Profile = accountRole.AccountID + "_" + accountRole.RoleName
			}
			stsCredential.AccountName = accountRole.AccountName
			stsCredentialSlice[i] = stsCredential

			return nil
		})
	}

	if err := g.Wait(); err != nil {
		return nil, fmt.Errorf("error running aws sso get-role-credentials: %s", err)
	}

	return stsCredentialSlice, nil
}

func checkPrefixHasHttps(output string) string {

	if strings.HasPrefix(output, "https://") {
		return output
	}

	if strings.HasPrefix(output, "http://") {
		return strings.Replace(output, "http://", "https://", 1)
	}

	return "https://" + output
}

func firstLine(output string) string {
	if i := strings.Index(output, "\n"); i >= 0 {
		return output[0:i]
	}
	return output
}

func parseJson(jsonOutput string) map[string]interface{} {
	var result map[string]interface{}
	_ = json.Unmarshal([]byte(jsonOutput), &result)
	return result
}

func parseAccounts(jsonOutput string, accounts *model.Accounts) {
	_ = json.Unmarshal([]byte(jsonOutput), &accounts)
}

func parseAccountRoles(jsonOutput string, accountRoles *model.AccountRoles) {
	_ = json.Unmarshal([]byte(jsonOutput), &accountRoles)
}

func parseStsCredential(jsonOutput string, stsCredential *model.StsCredential) {
	var result map[string]interface{}
	_ = json.Unmarshal([]byte(jsonOutput), &result)
	credentialsMap := result["roleCredentials"]
	values := credentialsMap.(map[string]interface{})
	stsCredential.AccessKeyID = values["accessKeyId"].(string)
	millis := int64(values["expiration"].(float64))
	stsCredential.Expiration = time.Unix(0, millis*int64(time.Millisecond))
	stsCredential.SecretAccessKey = values["secretAccessKey"].(string)
	stsCredential.SessionToken = values["sessionToken"].(string)
}
