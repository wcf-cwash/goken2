package info

import (
	"goken2/cmd"
	"goken2/config"
	"goken2/logging"
)

// GokenVersion prints the AWS CLI version along with the current Goken version when version flag or debug flag is set
func GokenVersion(config config.Config) {

	awsVersion, err := cmd.AwsVersion()
	if err != nil {
		awsVersion = "aws-cli version/UNAVAILABLE"
	}

	if config.PrintVersion || config.DebugMode {
		logging.Message.Printf("%s\nGoken version %s (Commit %s) \nBuilt on %s by %s",
			awsVersion,
			config.BuildVersion,
			config.BuildCommit,
			config.BuildDate,
			config.BuiltBy)
	}
}
