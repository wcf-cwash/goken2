package info

import (
	"goken2/config"
	"goken2/logging"
	"goken2/model"
)

type Printer struct {
	GokenConfig config.Config
}

func (p *Printer) Info(credentials []model.StsCredential) {
	maxLength := maxLength(credentials)

	if !p.GokenConfig.SilentMode {
		logging.Message.Println()
		logging.Message.Println("--------------------------------------------------------------------------------------------------")
		logging.Message.Println("    █████░░░    ")
		logging.Message.Println("   █      █░░      _____ _____ _   __ _____ _   _ ")
		logging.Message.Println("  █  ░░ █  █░░    |  __ \\  _  | | / /|  ___| \\ | |")
		logging.Message.Println(" █  ░   ██  █░░   | |  \\/ | | | |/ / | |__ |  \\| |")
		logging.Message.Println(" █  ░   ██  █░░   | | __| | | |    \\ |  __|| . ` |")
		logging.Message.Println(" █  ░   ██  █░░   | |_\\ \\ \\_/ / |\\  \\| |___| |\\  |")
		logging.Message.Println("  █  ████  █░░     \\____/\\___/\\_| \\_/\\____/\\_| \\_/")
		logging.Message.Println("   █      █░░   ")
		logging.Message.Println("    █████░░░    ")
		logging.Message.Println("--------------------------------------------------------------------------------------------------")
		logging.Message.Println("Yo dawg, I heard you like tokens. So I tokenized all of your STS Tokens in your ~/.aws/credentials")
		logging.Message.Println("file. I wrote tokens under the following profiles:")
		logging.Message.Println()
		for _, credential := range credentials {
			logging.Message.Println("PROFILE_NAME: " + credential.Profile + padding(credential.Profile, maxLength) + "|   ACCOUNT_NAME: " + credential.AccountName)
		}
		logging.Message.Println()
		logging.Message.Printf("Your tokens will self-destruct... I mean, _expire at_: %s \n", credentials[0].Expiration)
		logging.Message.Println()
		logging.Message.Println("To use one of these credentials, use the --profile option with the AWS CLI")
		logging.Message.Println(" (e.g. aws --profile <PROFILE_NAME> ec2 describe-instances)")
		logging.Message.Println()
		logging.Message.Println("Alternatively you could set up AWS profile completion in your shell: https://bit.ly/3bEKzsH")
		logging.Message.Println("--------------------------------------------------------------------------------------------------")
	}
}

func padding(profile string, maxLength int) string {
	var pad string
	for i := len(profile); i < maxLength+3; i++ {
		pad += " "
	}
	return pad
}

func maxLength(credentials []model.StsCredential) int {
	var maxLength int
	for _, credential := range credentials {
		if len(credential.Profile) > maxLength {
			maxLength = len(credential.Profile)
		}
	}
	return maxLength
}
