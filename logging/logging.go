// Package logging contains simple Loggers configured for different levels that will write to Stdout/Stderr or /dev/null
package logging

import (
	"goken2/config"
	"io"
	"io/ioutil"
	"log"
	"os"
)

var (
	// Trace logging used for very fine grained outputs, like starting and ending of calls or raw outputs
	Trace *log.Logger
	// Debug logging used when the program is run with -debug
	Debug *log.Logger
	// Profile logging used when the program is run with -profile to report important timings
	Profile *log.Logger
	// Info logging used when the program is run with no options for general messages
	Info *log.Logger
	// Message logging used to print a standard message to Stdout
	Message *log.Logger
	// Warning logging used when an event occurs that is not fatal but that may require user intervention or attention
	Warning *log.Logger
	// Error logging when program encounters event that is not expected or expected but requires user attention
	Error *log.Logger
)

// InitLogging will initialize all loggers based off of Config passed in
func InitLogging(config config.Config) {

	var traceHandle, debugHandle, profileHandle, infoHandle, messageHandle, warningHandle, errorHandle io.Writer

	traceHandle = ioutil.Discard // note, trace only enabled in source code by hand with recompile
	debugHandle = ioutil.Discard
	profileHandle = ioutil.Discard
	infoHandle = os.Stdout
	messageHandle = os.Stdout
	warningHandle = os.Stdout
	errorHandle = os.Stderr

	if config.DebugMode {
		debugHandle = os.Stdout
	}

	if config.SilentMode {
		infoHandle = ioutil.Discard
		warningHandle = ioutil.Discard
	}

	Trace = createLog(traceHandle, "TRACE: ")
	Debug = createLog(debugHandle, "DEBUG: ")
	Profile = createLog(profileHandle, "PROFILE: ")
	Info = createLog(infoHandle, "INFO: ")
	Message = createShortLog(messageHandle)
	Warning = createLog(warningHandle, "WARNING: ")
	Error = createLog(errorHandle, "ERROR: ")

}

func createShortLog(handle io.Writer) *log.Logger {
	return log.New(handle, "", 0)
}

func createLog(handle io.Writer, prefix string) *log.Logger {
	return log.New(handle, prefix,
		log.Ldate|log.Ltime|log.Lshortfile)
}
