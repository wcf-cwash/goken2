package model

import "time"

// StsCredential is an internal representation of the data we need to track related to STS credentials.
type StsCredential struct {
	Profile         string
	AccessKeyID     string    `json:"accessKeyId"`
	Expiration      time.Time `json:"expiration"`
	SecretAccessKey string    `json:"secretAccessKey"`
	SessionToken    string    `json:"sessionToken"`
	AccountName     string
}