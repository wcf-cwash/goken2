package model

type AccountRole struct {
	AccountID   string
	AccountName string
	RoleName    string `json:"roleName"`
}
