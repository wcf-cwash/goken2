package model

type Account struct {
	ID     string `json:"accountId"`
	Name   string `json:"accountName"`
}
