# goken2

This version of goken is based off of awscli2, using Vish Uma's goken.sh

It requires that you have `awscliv2` installed and will call it directly
through the command line.

The dependency on having a version of Chrome/Chromium for screen scraping
the MFA flow has been removed; it will still overwrite your
`~/aws/.credentials` file with proper STS tokens.  It will instead attempt
to pop open your default browser to enter credentials when run, after
successfully entering credentials you can return to your terminal to
complete the process of refreshing your tokens.

Using the SSO flow, it should support MFA devices like YubiKey and accept
credentials

## Installing

### MacOS

Install the AWS CLIv2:

```bash
brew install awscli
```

Make sure it is installed:

```bash
aws --version
```

#### Using Homebrew to Install Goken2

##### Removing Older Versions of Goken

**NOTE: If you have previously installed any prior versions of goken, please remove them!**

e.g., you should be able to run:

```bash
rm -rf /usr/local/bin/goken
```

##### Add Tap (1-time)

Because the `goken` forumla is not published to the public Homebrew repository, you should add a tap for it.
This will have your local homebrew track changes to the formula in this repo when you do a `brew update` in order to
manage upgrades to `goken` like all other Homebrew installed software.

```bash
brew tap wcf-cwash/goken https://gitlab.com/wcf-cwash/goken2
```

##### Prerequisites

Check that your `~/.aws` folder has `755` permissions

```bash
chmod 755 ~/.aws
```

`goken` needs to be able to write to files in this directory.

##### Install

```bash
brew update
brew install goken
```

##### Upgrade

```bash
brew upgrade goken
````

Note: if you are upgrading from a version prior to 1.1 you may need to retap.  During the upgrade, the name of the formula also changed back from `goken2` to `goken`. Use the commands to uninstall/retap/reinstall:

```
brew uninstall goken2
brew untap wcf-cwash/goken
brew tap wcf-cwash/goken https://gitlab.com/wcf-cwash/goken2.git
brew install goken
```

##### Test

If everything installed correctly, this command should run correctly and print the latest version information:

```bash
goken -version
```

See [Running Goken](#running-goken)



## Running Goken

When you run `goken` you may be prompted to select which browser will be opened; after selecting you will be redirected
to the standard SSO login screen.  Login as normal, and click 'Authorize'

You should have your `.credenitals` file updated with the appropriate profiles after successfully running the command;
you can check this by running:

```console
cat ~/.aws/credentials
```

If it pops a Finder window open, then something is misconfigured.

## Troubleshooting

If you see the following error, you will need to point your `AWS_CA_BUNDLE` variable at the appropriate certificate file:
` [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate (_ssl.c:1006)`

Find where your `awscli` is installed via Homebrew, it should be under
`/usr/local/Cellar/awscli/<YOUR_INSTALLED_CLI_VERSION>/libexec/lib/python3.11/site-packages/awscli/botocore/cacert.pem`

Export the variable in your shell (`.zshrc` for example)
```bash
export AWS_CA_BUNDLE=/usr/local/Cellar/awscli/2.13.21/libexec/lib/python3.11/site-packages/awscli/botocore/cacert.pem
```

You'll need to start a new Terminal for this to take affect.  NOTE that if you do a `brew upgrade` and a newer version of 
the CLI is installed you may need to also upgrade this reference to the directory.

TODO: come up with a command that uses a symlink for this (hopefully created/maintained by `brew`?) rather than an absolute reference

### set-aws-profile

If you use zsh and `oh-my-zsh` you probably want to install 
[these scripts](https://saurabh-hirani.github.io/writing/2018/04/05/zsh-and-aws-profiles) to allow you to easily switch
active profiles so that you don't need to provide them using `--profile` every time you run an AWS CLI command.

## goreleaser

See the [goreleaser Gitlab CI/CD docs](https://goreleaser.com/ci/gitlab/)
