package files

import (
	"github.com/bigkevmcd/go-configparser"
	"goken2/config"
	"goken2/utils"
	"os"
	"path"
	"time"
)

// Clear will remove all files produced by this application
func Clear(config config.Config) {
	if config.ClearFiles {
		localPath := awsPath()
		utils.Ignore(os.Rename(oldName(localPath, config.AwsConfigFile), newName(localPath, config.AwsConfigFile)))
		utils.Ignore(os.Rename(oldName(localPath, config.AwsCredentialsFile), newName(localPath, config.AwsCredentialsFile)))
	}
}

func oldName(localPath string, name string) string {
	return path.Join(localPath, name)
}

func newName(localPath string, name string) string {
	return path.Join(localPath, name+"_"+time.Now().Format("20060102150405")+".bak")
}

func checkAwsDirAndMakeIfNotExist() {
	localPath := awsPath()

	if _, err := os.Stat(localPath); os.IsNotExist(err) {
		_ = os.Mkdir(localPath, os.ModeDir)
	}
}

func awsPath() string {
	homeDir, err := os.UserHomeDir()
	utils.Ignore(err) // should be ignored so that we can check it (caller will make if it doesn't exist)
	joinedPath := path.Join(homeDir, ".aws")
	return joinedPath
}

func writeChanges(parser *configparser.ConfigParser, config config.Config, configFile string) error {

	actualFile := path.Join(awsPath(), configFile)

	err := parser.SaveWithDelimiter(actualFile, "=")
	utils.Check(err)

	_ = os.Chmod(actualFile, 0600)

	return nil
}
