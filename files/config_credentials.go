package files

import (
	"bytes"
	"fmt"
	"github.com/bigkevmcd/go-configparser"
	"goken2/config"
	"goken2/model"
	"goken2/utils"
	"io/ioutil"
	"path"
)

// WriteConfigAndCredentials will take an array of StsCredential structs and produce
// ~/.aws/config and ~/.aws/credential files with profiles that can be used when working
// with the AWS CLI tool (using --profile).
// These files are in .ini format and internally we delegate to the go-configparser library
// to update these files.
func WriteConfigAndCredentials(credentials []model.StsCredential, gokenConfig config.Config) error {

	awsConfigDefaults := make(configparser.Dict)
	awsConfigDefaults["scope"] = "urn:amazon:webservices"

	checkAwsDirAndMakeIfNotExist()

	configFileParser := configparser.NewWithDefaults(awsConfigDefaults)

	for _, credential := range credentials {
		sectionTitle := fmt.Sprintf("profile %s", credential.Profile)
		utils.Ignore(configFileParser.AddSection(sectionTitle))
		utils.Check(configFileParser.Set(sectionTitle, "output", "json"))
		utils.Check(configFileParser.Set(sectionTitle, "region", "us-east-1"))
	}

	// parse credentials
	credentialsFileParser := configparser.New()

	for _, credential := range credentials {
		utils.Ignore(credentialsFileParser.AddSection(credential.Profile))
		utils.Check(credentialsFileParser.Set(credential.Profile, "aws_access_key_id", credential.AccessKeyID))
		utils.Check(credentialsFileParser.Set(credential.Profile, "aws_secret_access_key", credential.SecretAccessKey))
		utils.Check(credentialsFileParser.Set(credential.Profile, "aws_session_token", credential.SessionToken))
		utils.Check(credentialsFileParser.Set(credential.Profile, "token_expiration", credential.Expiration.Format("2006-01-02T15:04:05Z")))
	}

	// write config
	writeConfigErr := writeChanges(configFileParser, gokenConfig, gokenConfig.AwsConfigFile)
	if writeConfigErr != nil {
		return writeConfigErr
	}

	// write credentials
	writeCredsErr := writeChanges(credentialsFileParser, gokenConfig, gokenConfig.AwsCredentialsFile)
	if writeCredsErr != nil {
		return writeCredsErr
	}

	replaceDefaultOG(gokenConfig)

	return nil
}

func replaceDefaultOG(gokenConfig config.Config) {
	credsFile := path.Join(awsPath(), gokenConfig.AwsCredentialsFile)
	input, readErr := ioutil.ReadFile(credsFile)
	utils.Check(readErr)

	output := bytes.Replace(input, []byte("[default-og]"), []byte("[default]"), -1)

	writeErr := ioutil.WriteFile(credsFile, output, 0600)
	utils.Check(writeErr)
}
