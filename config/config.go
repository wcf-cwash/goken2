package config

// Config encapsulates important state obtained from env vars and/or cmd line arguments
// and can be passed around to other parts of the application that are configurable
type Config struct {
	DebugMode          bool
	SilentMode         bool
	PrintVersion       bool
	AdminCreds         bool
	ClearFiles         bool
	AccountName        bool
	Region             string
	AwsConfigFile      string `default0:"config"`
	AwsCredentialsFile string `default0:"credentials"`
	Username           string
	BuildVersion       string
	BuildCommit        string
	BuildDate          string
	BuiltBy            string
}

// New will initialize a new Config with defaults
func New() Config {
	return Config{
		DebugMode:          false,
		SilentMode:         false,
		PrintVersion:       false,
		AdminCreds:         false,
		ClearFiles:         false,
		AccountName:        false,
		Region:             "us-east-1",
		AwsConfigFile:      "config",
		AwsCredentialsFile: "credentials",
		BuildVersion:       "n/a",
		BuildCommit:        "n/a",
		BuildDate:          "n/a",
		BuiltBy:            "n/a",
	}
}
